package com.ardinal.kotlinshoppingapp.model.entity

import com.google.gson.annotations.SerializedName

data class EntityUser (
    @field:SerializedName("Id", alternate = ["ID", "id", "iD"])
    val id : Int?,

    @field:SerializedName("username")
    val username: String?,

    @field:SerializedName("email", alternate = ["Email", "eMail"])
    val email: String?,

    @field:SerializedName("encrypted_password")
    val password: String?,

    @field:SerializedName("phone")
    val phone: String?,

    @field:SerializedName("address")
    val address: String?,

    @field:SerializedName("city")
    val city: String?,

    @field:SerializedName("country")
    val country: String?,

    @field:SerializedName("name")
    val name: String?,

    @field:SerializedName("postcode")
    val postcode: String?
)