package com.ardinal.kotlinshoppingapp.module.shop.main

import android.annotation.SuppressLint
import android.content.Intent
import com.ardinal.kotlinshoppingapp.module.shop.create.CreateShoppingActivity
import com.ardinal.kotlinshoppingapp.module.shop.get_all.ShoppingGetAllActivity
import com.ardinal.kotlinshoppingapp.module.shop.get_by_id.ShoppingGetByIdActivity
import com.ardinal.kotlinshoppingapp.module.user.get_all.UserGetAllActivity

class ShoppingMainRouter : ShoppingMainContracts.ShoppingMainPresenterToRouterInterface {

	companion object {
		@SuppressLint("StaticFieldLeak")
		var view: ShoppingMainActivity? = null

		fun configure(activity: ShoppingMainActivity) {
			val presenter = ShoppingMainPresenter()
			val interactor = ShoppingMainInteractor()
			val router = ShoppingMainRouter()

			activity.presenter = presenter
			presenter.view = activity
			presenter.router = router
			presenter.interector = interactor
			interactor.presenter = presenter
			view = activity
		}
	}

	override fun gotoAllUser() {
		val intent = Intent(view?.context, UserGetAllActivity::class.java)
		view?.startActivity(intent)
	}

	override fun gotoCreateShopping() {
		val intent = Intent(view?.context, CreateShoppingActivity::class.java)
		view?.startActivity(intent)
	}

	override fun gotoAllShopping() {
		val intent = Intent(view?.context, ShoppingGetAllActivity::class.java)
		view?.startActivity(intent)
	}

	override fun gotoByIdShopping() {
		val intent = Intent(view?.context, ShoppingGetByIdActivity::class.java)
		view?.startActivity(intent)
	}
}