package com.ardinal.kotlinshoppingapp.module.user.sign_in

import android.os.Bundle
import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import com.ardinal.kotlinshoppingapp.R;
import com.ardinal.kotlinshoppingapp.base.BaseView
import com.ardinal.kotlinshoppingapp.model.request.SignInRequest
import com.ardinal.kotlinshoppingapp.model.response.AuthResponse
import kotlinx.android.synthetic.main.activity_user_sign_in.*
import kotlinx.android.synthetic.main.component_toolbar.*

class SignInActivity : BaseView(), SignInContracts.SignInPresenterToViewInterface, View.OnClickListener {

	// MARK: Properties
	override var presenter: SignInPresenter? = null
	override val context: Context = this

	// MARK: Lifecycle
	override fun onCreate(savedInstanceState: Bundle?) {
		SignInRouter.configure(this)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_user_sign_in)

		setupView()
	}

	override fun setupView() {
		toolbar_title.text = getString(R.string.sign_in)

		setupLink()
	}

	override fun setupLink() {
		toolbar_back_button.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		sign_in_submit.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}
	}

	override fun onClick(view: View?) {
		when(view?.id) {
			sign_in_submit.id -> {
				commitSignIn()
			}
			toolbar_back_button.id -> {
				onBackPressed()
			}
		}
	}

	private fun getData(): SignInRequest? {
		val email = sign_in_email.text.toString()
		val password = sign_in_password.text.toString()

		return if(email == "" || password == "") {
			null
		} else {
			SignInRequest(email, password)
		}
	}

	private fun commitSignIn() {
		val request = getData()
		if (request != null) {
			Log.d("MY_DATA", request.toString())
			presenter?.interector?.submitSignIn(request)
		}
		else {

		}
	}
}
