package com.ardinal.kotlinshoppingapp.module.user.sign_up

import android.annotation.SuppressLint
import android.util.Log
import com.ardinal.kotlinshoppingapp.api.ShoppingAPI
import com.ardinal.kotlinshoppingapp.base.BaseAPI
import com.ardinal.kotlinshoppingapp.model.request.SignUpRequest
import io.reactivex.android.schedulers.AndroidSchedulers

class SignUpInteractor : SignUpContracts.SignUpPresentorToInteractorInterface {
    override var presenter: SignUpPresenter? = null

    @SuppressLint("CheckResult")
    override fun submitSignUp(request: SignUpRequest) {

        BaseAPI.instance.createRequest(false)!!.create(ShoppingAPI::class.java)
            .requestSignUp(request).observeOn(AndroidSchedulers.mainThread()).subscribe({
                result ->

                if (result.isSuccessful) {
                    val response = result.body()
                    Log.d("RESULT_CONTENT", response?.toString())
                    presenter?.responseSuccess(response)
                }
                else {
                    presenter?.responseFailed()
                }
            }, {
                    e -> e.printStackTrace()
            })
    }
}
