package com.ardinal.kotlinshoppingapp.module.user.sign_in

import android.annotation.SuppressLint
import android.content.Context
import com.ardinal.kotlinshoppingapp.model.request.SignInRequest
import com.ardinal.kotlinshoppingapp.model.response.AuthResponse

object SignInContracts {

	interface SignInPresenterToViewInterface {
		val context: Context
		var presenter: SignInPresenter?

		fun setupView()
		fun setupLink()
	}

	interface SignInPresentorToInteractorInterface {
		var presenter: SignInPresenter?

		fun submitSignIn(request: SignInRequest)
	}

	interface SignInInteractorToPresenterInterface {

		fun responseSuccess(data: AuthResponse?)
		fun responseFailed()
	}

	interface SignInViewToPresenterInterface {
		var view: SignInActivity?
		var interector: SignInInteractor?
		var router: SignInRouter?

	}

	interface SignInPresenterToRouterInterface {
		companion object {
			@SuppressLint("StaticFieldLeak")
			var view: SignInActivity? = null

			fun configure() {}
		}

		fun gotoShoppingMain()
	}
}
