package com.ardinal.kotlinshoppingapp.module.shop.get_by_id

import android.annotation.SuppressLint
import android.util.Log
import com.ardinal.kotlinshoppingapp.api.ShoppingAPI
import com.ardinal.kotlinshoppingapp.base.BaseAPI
import io.reactivex.android.schedulers.AndroidSchedulers

class ShoppingGetByIdInteractor : ShoppingGetByIdContracts.ShoppingGetByIdPresentorToInteractorInterface {
	override var presenter: ShoppingGetByIdPresenter? = null

	@SuppressLint("CheckResult")
	override fun fetchShoppingByID(request: Int) {
		BaseAPI.instance.createRequest(true)!!.create(ShoppingAPI::class.java)
			.getByIdShopping(request).observeOn(AndroidSchedulers.mainThread()).subscribe({
				result ->

				if(result.isSuccessful) {
					val response = result.body()
					Log.d("RESULT_CONTENT", response?.data.toString())
					Log.d("RESULT_MESSAGE", response?.message.toString())
					presenter?.responseSuccess(response?.data)
				}
				else {
					presenter?.responseFailed()
				}
			}, {
					e -> e.printStackTrace()
			})
	}
}
