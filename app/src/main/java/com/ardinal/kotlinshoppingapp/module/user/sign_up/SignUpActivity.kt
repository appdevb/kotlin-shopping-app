package com.ardinal.kotlinshoppingapp.module.user.sign_up

import android.os.Bundle
import android.content.Context
import android.view.View
import com.ardinal.kotlinshoppingapp.R;
import com.ardinal.kotlinshoppingapp.base.BaseView
import com.ardinal.kotlinshoppingapp.model.entity.EntityUser
import com.ardinal.kotlinshoppingapp.model.request.SignUpRequest
import com.ardinal.kotlinshoppingapp.model.response.AuthResponse
import kotlinx.android.synthetic.main.activity_user_sign_up.*
import kotlinx.android.synthetic.main.component_toolbar.*

class SignUpActivity : BaseView(), SignUpContracts.SignUpPresenterToViewInterface, View.OnClickListener {

    // MARK: Properties
    override var presenter: SignUpPresenter? = null
    override val context: Context = this

	private var user: EntityUser? = null

    // MARK: Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        SignUpRouter.configure(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_sign_up)

	    setupView()
    }

	override fun setupView() {
		toolbar_title.text = getString(R.string.sign_up)

		setupLink()
	}

	override fun setupLink() {
		toolbar_back_button.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		sign_up_submit.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}
	}

	override fun onClick(view: View?) {
		when (view?.id) {
			toolbar_back_button.id -> {
				onBackPressed()
			}
			sign_up_submit.id -> {
				createUser()
			}
		}
	}

	private fun getData() : EntityUser? {
		val username = sign_up_username.text.toString()
		val name = sign_up_name.text.toString()
		val email = sign_up_email.text.toString()
		val password = sign_up_password.text.toString()
		val phone = sign_up_phone.text.toString()
		val address = sign_up_address.text.toString()
		val city = sign_up_city.text.toString()
		val country = sign_up_country.text.toString()
		val postcode = sign_up_postcode.text.toString()

		return if ((username == "") ||(name == "") ||(email == "") || (password == "") || (phone == "") ||
			(address == "") || (city == "") || (country == "") || (postcode == "")) {
			null
		} else {
			EntityUser(null, username, email, password, phone, address, city, country, name, postcode)
		}
	}

	private fun createUser() {
		val data = getData()

		if (data != null) {
			val request = SignUpRequest(data)
			presenter?.interector?.submitSignUp(request)
		}
		else {

		}
	}

	fun showResponse(data: AuthResponse?) {
		if (data?.token != null) {

		}
		else {

		}
	}
}
