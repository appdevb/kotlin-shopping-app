package com.ardinal.kotlinshoppingapp.module.user.sign_in

import android.annotation.SuppressLint
import android.util.Log
import com.ardinal.kotlinshoppingapp.api.ShoppingAPI
import com.ardinal.kotlinshoppingapp.base.BaseAPI
import com.ardinal.kotlinshoppingapp.model.request.SignInRequest
import io.reactivex.android.schedulers.AndroidSchedulers

class SignInInteractor : SignInContracts.SignInPresentorToInteractorInterface {

	override var presenter: SignInPresenter? = null

	@SuppressLint("CheckResult")
	override fun submitSignIn(request: SignInRequest) {
		Log.d("MY_DATA", request.toString())
		BaseAPI.instance.createRequest(false)!!.create(ShoppingAPI::class.java)
			.requestSignIn(request).observeOn(AndroidSchedulers.mainThread()).subscribe({
				result ->

				if(result.isSuccessful) {
					val response = result.body()
					Log.d("RESULT_CONTENT", response?.toString())
					presenter?.responseSuccess(response)
				}
				else {
					presenter?.responseFailed()
				}
			}, {
					e -> e.printStackTrace()
			})
	}
}
