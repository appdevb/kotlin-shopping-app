package com.ardinal.kotlinshoppingapp.module.shop.get_all

import android.annotation.SuppressLint
import android.util.Log
import com.ardinal.kotlinshoppingapp.api.ShoppingAPI
import com.ardinal.kotlinshoppingapp.base.BaseAPI
import io.reactivex.android.schedulers.AndroidSchedulers

class ShoppingGetAllInteractor : ShoppingGetAllContracts.ShoppingGetAllPresentorToInteractorInterface {

	override var presenter: ShoppingGetAllPresenter? = null

	@SuppressLint("CheckResult")
	override fun fetchAllShopping() {
		BaseAPI.instance.createRequest(true)!!.create(ShoppingAPI::class.java)
			.requestAllShopping().observeOn(AndroidSchedulers.mainThread()).subscribe({
					result ->

				if(result.isSuccessful) {
					val response = result.body()
					Log.d("RESULT_CONTENT", response?.data.toString())
					Log.d("RESULT_MESSAGE", response?.message.toString())
					presenter?.responseSuccess(response?.data)
				}
				else {
					presenter?.responseFailed()
				}
			}, {
					e -> e.printStackTrace()
			})
	}

	@SuppressLint("CheckResult")
	override fun submitDeleteShopping(id: Int) {
		BaseAPI.instance.createRequest(true)!!.create(ShoppingAPI::class.java)
			.requestDeleteShopping(id).observeOn(AndroidSchedulers.mainThread()).subscribe({
					result ->

				if(result.isSuccessful) {
					val response = result.body()
					Log.d("RESULT_CONTENT", response?.data.toString())
					presenter?.responseDeleteSuccess(response?.data)
				}
				else {
					presenter?.responseFailed()
				}
			}, {
					e -> e.printStackTrace()
			})
	}

}
