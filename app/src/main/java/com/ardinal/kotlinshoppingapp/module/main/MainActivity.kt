package com.ardinal.kotlinshoppingapp.module.main

import android.content.Context
import android.os.Bundle
import android.view.View
import com.ardinal.kotlinshoppingapp.R
import com.ardinal.kotlinshoppingapp.base.BaseView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.component_toolbar.*

class MainActivity : BaseView(), MainContracts.MainPresenterToViewInterface, View.OnClickListener {

    // MARK: Properties
    override var presenter: MainPresenter? = null
    override val context: Context = this

    // MARK: Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        MainRouter.configure(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

	    setupView()
    }

	override fun setupView() {
		toolbar_back_button.visibility = View.GONE
		toolbar_title.text = getString(R.string.main_menu)
		setupLink()
	}

	override fun setupLink() {
		main_btn_signup.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		main_btn_signin.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}
	}

	override fun onClick(view: View?) {
		when(view?.id) {
			main_btn_signup.id -> {
				presenter?.router?.gotoSignUp()
			}
			main_btn_signin.id -> {
				presenter?.router?.gotoSignIn()
			}
		}
	}
}
