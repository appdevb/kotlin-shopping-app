package com.ardinal.kotlinshoppingapp.module.shop.get_by_id

import android.annotation.SuppressLint

class ShoppingGetByIdRouter : ShoppingGetByIdContracts.ShoppingGetByIdPresenterToRouterInterface {

	companion object {
		@SuppressLint("StaticFieldLeak")
		var view: ShoppingGetByIdActivity? = null

		fun configure(activity: ShoppingGetByIdActivity) {
			val presenter = ShoppingGetByIdPresenter()
			val interactor = ShoppingGetByIdInteractor()
			val router = ShoppingGetByIdRouter()

			activity.presenter = presenter
			presenter.view = activity
			presenter.router = router
			presenter.interector = interactor
			interactor.presenter = presenter
			view = activity
		}
	}
}