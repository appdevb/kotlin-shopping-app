package com.ardinal.kotlinshoppingapp.module.shop.create

import android.os.Bundle
import android.content.Context
import android.view.View
import com.ardinal.kotlinshoppingapp.R;
import com.ardinal.kotlinshoppingapp.base.BaseView
import com.ardinal.kotlinshoppingapp.model.entity.EntityShop
import com.ardinal.kotlinshoppingapp.model.request.ActionShoppingRequest
import kotlinx.android.synthetic.main.activity_shopping_create.*
import kotlinx.android.synthetic.main.activity_shopping_main.*
import kotlinx.android.synthetic.main.component_toolbar.*

class CreateShoppingActivity : BaseView(), CreateShoppingContracts.CreateShoppingPresenterToViewInterface, View.OnClickListener {

	// MARK: Properties
	override var presenter: CreateShoppingPresenter? = null
	override val context: Context = this

	// MARK: Lifecycle
	override fun onCreate(savedInstanceState: Bundle?) {
		CreateShoppingRouter.configure(this)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_shopping_create)

		setupView()
	}

	override fun setupView() {
		toolbar_title.text = getString(R.string.create_new_shopping)

		setupLink()
	}

	override fun setupLink() {
		toolbar_back_button.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		shop_create_date.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		shop_create_submit.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}
	}

	override fun onClick(view: View?) {
		when(view?.id) {
			toolbar_back_button.id -> {
				onBackPressed()
			}
			shop_create_date.id -> {
				showDatePicker(this, shop_create_date)
			}
			shop_create_submit.id -> {
				createShopping()
			}
		}
	}

	private fun getData() : ActionShoppingRequest? {
		val date = shop_create_date.text.toString()
		val name = shop_create_name.text.toString()

		return if (date == "" || name == "") {
			null
		} else {
			val data = EntityShop(null, name, date)
			ActionShoppingRequest(data)
		}
	}

	private fun createShopping() {
		val request = getData()

		if(request != null) {
			presenter?.interector?.submitCreateShopping(request)
		}
		else {
			showToast(this, "Mohon isi semua kolom isian")
		}
	}

	fun showResponse(data: EntityShop?) {
		if(data != null) {
			val message = "Data baru = nama: ${data.name}, tanggal: ${data.date}"
			showToast(this, message)
			presenter?.router?.gotoShoppingAll()
		}
		else {
			showToast(this, "Data baru gagal ditambahkan")
		}
	}
}
