package com.ardinal.kotlinshoppingapp.module.main

import android.annotation.SuppressLint
import android.content.Intent
import com.ardinal.kotlinshoppingapp.module.user.sign_in.SignInActivity
import com.ardinal.kotlinshoppingapp.module.user.sign_up.SignUpActivity

class MainRouter : MainContracts.MainPresenterToRouterInterface {

    companion object {
        @SuppressLint("StaticFieldLeak")
        var view: MainActivity? = null

        fun configure(activity: MainActivity) {
            val presenter = MainPresenter()
            val interactor = MainInteractor()
            val router = MainRouter()

            activity.presenter = presenter
            presenter.view = activity
            presenter.router = router
            presenter.interector = interactor
            interactor.presenter = presenter
            view = activity
        }
    }

    override fun gotoSignUp() {
        val intent = Intent(view?.context, SignUpActivity::class.java)
        view?.startActivity(intent)
    }

    override fun gotoSignIn() {
        val intent = Intent(view?.context, SignInActivity::class.java)
        view?.startActivity(intent)
    }
}