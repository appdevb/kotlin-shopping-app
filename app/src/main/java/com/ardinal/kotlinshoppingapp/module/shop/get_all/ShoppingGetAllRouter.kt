package com.ardinal.kotlinshoppingapp.module.shop.get_all

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import com.ardinal.kotlinshoppingapp.module.shop.update.ShoppingUpdateActivity

class ShoppingGetAllRouter : ShoppingGetAllContracts.ShoppingGetAllPresenterToRouterInterface {

	private val bundle = Bundle()

	companion object {
		@SuppressLint("StaticFieldLeak")
		var view: ShoppingGetAllActivity? = null

		fun configure(activity: ShoppingGetAllActivity) {
			val presenter = ShoppingGetAllPresenter()
			val interactor = ShoppingGetAllInteractor()
			val router = ShoppingGetAllRouter()

			activity.presenter = presenter
			presenter.view = activity
			presenter.router = router
			presenter.interector = interactor
			interactor.presenter = presenter
			view = activity
		}
	}

	override fun gotoUpdateShopping(id: Int) {
		val intent = Intent(view?.context, ShoppingUpdateActivity::class.java)
		bundle.putInt("KEY_UPDATE", id)
		intent.putExtras(bundle)
		view?.startActivity(intent)
	}
}