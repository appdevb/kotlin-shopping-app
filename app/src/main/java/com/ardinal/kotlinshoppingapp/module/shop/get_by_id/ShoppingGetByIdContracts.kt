package com.ardinal.kotlinshoppingapp.module.shop.get_by_id

import android.annotation.SuppressLint
import android.content.Context
import com.ardinal.kotlinshoppingapp.model.entity.EntityShop

object ShoppingGetByIdContracts {

	interface ShoppingGetByIdPresenterToViewInterface {
		val context: Context
		var presenter: ShoppingGetByIdPresenter?

		fun setupView()
		fun setupLink()
	}

	interface ShoppingGetByIdPresentorToInteractorInterface {
		var presenter: ShoppingGetByIdPresenter?

		fun fetchShoppingByID(request: Int)
	}

	interface ShoppingGetByIdInteractorToPresenterInterface {
		fun responseSuccess(data: EntityShop?)
		fun responseFailed()
	}

	interface ShoppingGetByIdViewToPresenterInterface {
		var view: ShoppingGetByIdActivity?
		var interector: ShoppingGetByIdInteractor?
		var router: ShoppingGetByIdRouter?

	}

	interface ShoppingGetByIdPresenterToRouterInterface {
		companion object {
			@SuppressLint("StaticFieldLeak")
			var view: ShoppingGetByIdActivity? = null

			fun configure() {}
		}
	}
}
