package com.ardinal.kotlinshoppingapp.module.shop.create

import android.annotation.SuppressLint
import android.content.Intent
import com.ardinal.kotlinshoppingapp.module.shop.get_all.ShoppingGetAllActivity

class CreateShoppingRouter : CreateShoppingContracts.CreateShoppingPresenterToRouterInterface {

	companion object {
		@SuppressLint("StaticFieldLeak")
		var view: CreateShoppingActivity? = null

		fun configure(activity: CreateShoppingActivity) {
			val presenter = CreateShoppingPresenter()
			val interactor = CreateShoppingInteractor()
			val router = CreateShoppingRouter()

			activity.presenter = presenter
			presenter.view = activity
			presenter.router = router
			presenter.interector = interactor
			interactor.presenter = presenter
			view = activity
		}
	}

	override fun gotoShoppingAll() {
		val intent = Intent(view?.context, ShoppingGetAllActivity()::class.java)
		view?.startActivity(intent)
	}
}