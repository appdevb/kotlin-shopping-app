package com.ardinal.kotlinshoppingapp.util

import com.ardinal.kotlinshoppingapp.model.entity.Auth
import com.orhanobut.hawk.Hawk

class Preferences {

	private val PREF_TOKEN = "TOKEN"

	companion object {
		var instance = Preferences()
	}

	val userInfo: String
		get() = Hawk.get(PREF_TOKEN)

	fun setUserInfo(data: Auth) {
		Hawk.put(PREF_TOKEN, data.token)
	}
}