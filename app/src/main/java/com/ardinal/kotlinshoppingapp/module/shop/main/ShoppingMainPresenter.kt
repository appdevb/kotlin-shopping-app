package com.ardinal.kotlinshoppingapp.module.shop.main

class ShoppingMainPresenter : ShoppingMainContracts.ShoppingMainViewToPresenterInterface,
	ShoppingMainContracts.ShoppingMainInteractorToPresenterInterface {

	override var view: ShoppingMainActivity? = null
	override var interector: ShoppingMainInteractor? = null
	override var router: ShoppingMainRouter? = null

}
