package com.ardinal.kotlinshoppingapp.module.shop.main

import android.annotation.SuppressLint
import android.content.Context

object ShoppingMainContracts {

	interface ShoppingMainPresenterToViewInterface {
		val context: Context
		var presenter: ShoppingMainPresenter?

		fun setupView()
		fun setupLink()
	}

	interface ShoppingMainPresentorToInteractorInterface {
		var presenter: ShoppingMainPresenter?
	}

	interface ShoppingMainInteractorToPresenterInterface

	interface ShoppingMainViewToPresenterInterface {
		var view: ShoppingMainActivity?
		var interector: ShoppingMainInteractor?
		var router: ShoppingMainRouter?

	}

	interface ShoppingMainPresenterToRouterInterface {
		companion object {
			@SuppressLint("StaticFieldLeak")
			var view: ShoppingMainActivity? = null

			fun configure(activity: ShoppingMainActivity) {}
		}

		fun gotoAllUser()
		fun gotoCreateShopping()
		fun gotoAllShopping()
		fun gotoByIdShopping()
	}

}
