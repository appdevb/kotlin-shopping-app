package com.ardinal.kotlinshoppingapp.module.shop.get_all.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ardinal.kotlinshoppingapp.R
import com.ardinal.kotlinshoppingapp.model.entity.EntityShop
import com.ardinal.kotlinshoppingapp.module.shop.get_all.ShoppingGetAllActivity
import kotlinx.android.synthetic.main.component_list_shopping.view.*


class AllShoppingAdapter (var data: List<EntityShop> = listOf()) : RecyclerView.Adapter<AllShoppingViewHolder>() {

	override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): AllShoppingViewHolder {
		return AllShoppingViewHolder(
			LayoutInflater.from(viewGroup.context).inflate(R.layout.component_list_shopping, viewGroup, false)
		)
	}

	override fun getItemCount(): Int {
		return data.size
	}

	override fun onBindViewHolder(holder: AllShoppingViewHolder, position: Int) {
		holder.bindData(data[position])
	}
}

class AllShoppingViewHolder(view: View) : RecyclerView.ViewHolder(view) {

	fun bindData(data: EntityShop) {
		itemView.apply {
			list_shopping_id.text = data.id.toString()
			list_shopping_name.text = data.name
			list_shopping_date.text = data.date
		}

		val act = itemView.context as ShoppingGetAllActivity
		itemView.list_shopping_update.setOnClickListener {
			act.onUpdateClicked(data.id!!)
		}

		itemView.list_shopping_delete.setOnClickListener {
			act.onDeleteClicked(data.id!!)
		}
	}
}