package com.ardinal.kotlinshoppingapp.module.main

class MainPresenter : MainContracts.MainViewToPresenterInterface, MainContracts.MainInteractorToPresenterInterface {

    override var view: MainActivity? = null
    override var interector: MainInteractor? = null
    override var router: MainRouter? = null

}
