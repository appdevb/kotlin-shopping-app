package com.ardinal.kotlinshoppingapp.module.shop.get_all

import com.ardinal.kotlinshoppingapp.model.entity.EntityShop

class ShoppingGetAllPresenter : ShoppingGetAllContracts.ShoppingGetAllViewToPresenterInterface,
	ShoppingGetAllContracts.ShoppingGetAllInteractorToPresenterInterface {

	override var view: ShoppingGetAllActivity? = null
	override var interector: ShoppingGetAllInteractor? = null
	override var router: ShoppingGetAllRouter? = null

	override fun responseSuccess(data: List<EntityShop>?) {
		view?.showResponse(data)
	}

	override fun responseDeleteSuccess(data: String?) {
		view?.showToast(view?.context!!,"Hapus Berhasil")
		view?.recreate()
	}

	override fun responseFailed() {
		view?.showToast(view?.context!!,"Gagal mendapatkan response dari server")
	}
}
