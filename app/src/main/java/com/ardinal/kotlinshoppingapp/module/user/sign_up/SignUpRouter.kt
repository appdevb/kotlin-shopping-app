package com.ardinal.kotlinshoppingapp.module.user.sign_up

import android.annotation.SuppressLint
import android.content.Intent
import com.ardinal.kotlinshoppingapp.module.shop.main.ShoppingMainActivity
import com.ardinal.kotlinshoppingapp.module.user.sign_in.SignInRouter

class SignUpRouter : SignUpContracts.SignUpPresenterToRouterInterface {

    companion object {
        @SuppressLint("StaticFieldLeak")
        var view: SignUpActivity? = null

        fun configure(activity: SignUpActivity) {
            val presenter = SignUpPresenter()
            val interactor = SignUpInteractor()
            val router = SignUpRouter()

            activity.presenter = presenter
            presenter.view = activity
            presenter.router = router
            presenter.interector = interactor
            interactor.presenter = presenter
            view = activity
        }
    }

    override fun gotoShoppingMain() {
        val intent = Intent(view?.context, ShoppingMainActivity::class.java)
        view?.startActivity(intent)
    }
}