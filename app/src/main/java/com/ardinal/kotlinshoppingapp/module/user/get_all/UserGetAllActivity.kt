package com.ardinal.kotlinshoppingapp.module.user.get_all

import android.os.Bundle
import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.ardinal.kotlinshoppingapp.R;
import com.ardinal.kotlinshoppingapp.base.BaseView
import com.ardinal.kotlinshoppingapp.model.entity.EntityUser
import com.ardinal.kotlinshoppingapp.module.user.get_all.adapter.AllUserAdapter
import kotlinx.android.synthetic.main.activity_get_all.*
import kotlinx.android.synthetic.main.component_toolbar.*

class UserGetAllActivity : BaseView(), UserGetAllContracts.UserGetAllPresenterToViewInterface, View.OnClickListener {

	// MARK: Properties
	override var presenter: UserGetAllPresenter? = null
	override val context: Context = this

	private val dataAdapter = AllUserAdapter()

	// MARK: Lifecycle
	override fun onCreate(savedInstanceState: Bundle?) {
		UserGetAllRouter.configure(this)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_get_all)

		setupView()
	}

	override fun setupView() {
		toolbar_title.text = getString(R.string.get_all_user)

		setupLink()
		setupRecyclerView()
		presenter?.interector?.fetchAllUser()
	}

	override fun setupLink() {
		toolbar_back_button.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}
	}

	override fun onClick(view: View?) {
		when(view?.id) {
			toolbar_back_button.id -> {
				onBackPressed()
			}
		}
	}

	private fun setupRecyclerView() {
		get_all_recycler_view.apply {
			layoutManager = LinearLayoutManager(this@UserGetAllActivity)
			adapter = dataAdapter
		}
	}

	fun showResponse(data: List<EntityUser>?) {
		if (data != null) {
			dataAdapter.data = data
			dataAdapter.notifyDataSetChanged()
		}
	}
}
