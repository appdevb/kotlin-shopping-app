package com.ardinal.kotlinshoppingapp.module.shop.update

import android.annotation.SuppressLint
import android.content.Context
import com.ardinal.kotlinshoppingapp.model.entity.EntityShop
import com.ardinal.kotlinshoppingapp.model.request.ActionShoppingRequest

object ShoppingUpdateContracts {

	interface ShoppingUpdatePresenterToViewInterface {
		val context: Context
		var presenter: ShoppingUpdatePresenter?

		fun setupView()
		fun setupLink()
	}

	interface ShoppingUpdatePresentorToInteractorInterface {
		var presenter: ShoppingUpdatePresenter?

		fun submitUpdateShopping(id: Int?, request: ActionShoppingRequest?)
	}

	interface ShoppingUpdateInteractorToPresenterInterface {
		fun responseSuccess(data: EntityShop?)
		fun responseFailed()
	}

	interface ShoppingUpdateViewToPresenterInterface {
		var view: ShoppingUpdateActivity?
		var interector: ShoppingUpdateInteractor?
		var router: ShoppingUpdateRouter?

	}

	interface ShoppingUpdatePresenterToRouterInterface {
		companion object {
			@SuppressLint("StaticFieldLeak")
			var view: ShoppingUpdateActivity? = null

			fun configure() {}
		}
		fun gotoAllShopping()
	}

}
