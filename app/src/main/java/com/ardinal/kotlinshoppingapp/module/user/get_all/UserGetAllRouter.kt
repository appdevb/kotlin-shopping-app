package com.ardinal.kotlinshoppingapp.module.user.get_all

class UserGetAllRouter : UserGetAllContracts.UserGetAllPresenterToRouterInterface {

	companion object {
		var view: UserGetAllActivity? = null

		fun configure(activity: UserGetAllActivity) {
			val presenter = UserGetAllPresenter()
			val interactor = UserGetAllInteractor()
			val router = UserGetAllRouter()

			activity.presenter = presenter
			presenter.view = activity
			presenter.router = router
			presenter.interector = interactor
			interactor.presenter = presenter
			view = activity
		}
	}
}