package com.ardinal.kotlinshoppingapp.util

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.view.View
import android.widget.DatePicker
import android.widget.EditText
import java.text.SimpleDateFormat
import java.util.*

class CalendarUtil(var context: Context, var field: EditText) : View.OnClickListener, DatePickerDialog.OnDateSetListener {

	private val c = Calendar.getInstance(TimeZone.getDefault())

	init {
		context as Activity
		field.setOnClickListener(this)
	}

	override fun onClick(v:View) {
		val year = c.get(Calendar.YEAR)
		val month = c.get(Calendar.MONTH)
		val day = c.get(Calendar.DAY_OF_MONTH)
		val dialog = DatePickerDialog(context, this, year, month, day)
		dialog.show()
	}

	override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
		c.set(Calendar.YEAR, year)
		c.set(Calendar.MONTH, month)
		c.set(Calendar.DAY_OF_MONTH, dayOfMonth)

		updateField()
	}

	private fun updateField() {
		val myFormat = "yyyy-MM-dd"
		val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
		field.setText(sdf.format(c.time))
	}
}