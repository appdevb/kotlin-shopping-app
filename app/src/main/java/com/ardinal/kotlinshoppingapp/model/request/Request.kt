package com.ardinal.kotlinshoppingapp.model.request

import com.ardinal.kotlinshoppingapp.model.entity.EntityShop
import com.ardinal.kotlinshoppingapp.model.entity.EntityUser
import com.google.gson.annotations.SerializedName

data class SignUpRequest (
	@field:SerializedName("user")
	var user: EntityUser?
)

data class SignInRequest (
	@field:SerializedName("email")
	var email: String?,

	@field:SerializedName("password")
	var password: String?
)

data class ActionShoppingRequest (
	@field:SerializedName("shopping")
	var shopping: EntityShop
)