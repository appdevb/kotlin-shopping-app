package com.ardinal.kotlinshoppingapp.module.user.sign_in

import com.ardinal.kotlinshoppingapp.model.entity.Auth
import com.ardinal.kotlinshoppingapp.model.response.AuthResponse
import com.ardinal.kotlinshoppingapp.util.Preferences

class SignInPresenter : SignInContracts.SignInViewToPresenterInterface,
	SignInContracts.SignInInteractorToPresenterInterface {

	override var view: SignInActivity? = null
	override var interector: SignInInteractor? = null
	override var router: SignInRouter? = null

	override fun responseSuccess(data: AuthResponse?) {
		if (data != null) {
			val auth = Auth(data.token)
			Preferences.instance.setUserInfo(auth)
			router?.gotoShoppingMain()
		}
		else {
			view?.showToast(view?.context!!,"Gagal mendapatkan authorized")
		}
	}

	override fun responseFailed() {
		view?.showToast(view?.context!!,"Gagal mendapatkan response dari server")

	}

}
