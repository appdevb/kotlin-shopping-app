package com.ardinal.kotlinshoppingapp.api

import com.ardinal.kotlinshoppingapp.model.request.ActionShoppingRequest
import com.ardinal.kotlinshoppingapp.model.request.SignInRequest
import com.ardinal.kotlinshoppingapp.model.request.SignUpRequest
import com.ardinal.kotlinshoppingapp.model.response.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*


interface ShoppingAPI {

	@POST("signup")
	fun requestSignUp(@Body body: SignUpRequest): Observable<Response<AuthResponse>>

	@POST("signin")
	fun requestSignIn(@Body body: SignInRequest): Observable<Response<AuthResponse>>

	@GET("users")
	fun requestAllUser(): Observable<Response<AllUserResponse>>

	@POST("shopping")
	fun requestCreateShopping(@Body body: ActionShoppingRequest): Observable<Response<ActionShoppingResponse>>

	@GET("shopping")
	fun requestAllShopping(): Observable<Response<AllShoppingResponse>>

	@GET("shopping/{id}")
	fun getByIdShopping(@Path("id") body: Int): Observable<Response<ByIdShoppingResponse>>

	@PUT("shopping/{id}")
	fun requestUpdateShopping(@Path("id") id: Int, @Body body: ActionShoppingRequest): Observable<Response<ActionShoppingResponse>>

	@DELETE("shopping/{id}")
	fun requestDeleteShopping(@Path("id") id: Int): Observable<Response<DeleteShoppingResponse>>
}