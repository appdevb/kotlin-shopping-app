package com.ardinal.kotlinshoppingapp.module.user.sign_up

import android.annotation.SuppressLint
import android.content.Context
import com.ardinal.kotlinshoppingapp.model.request.SignUpRequest
import com.ardinal.kotlinshoppingapp.model.response.AuthResponse

object SignUpContracts {

    interface SignUpPresenterToViewInterface {
        val context: Context
        var presenter: SignUpPresenter?

        fun setupView()
        fun setupLink()
    }

    interface SignUpPresentorToInteractorInterface {
        var presenter: SignUpPresenter?

        fun submitSignUp(request: SignUpRequest)
    }

    interface SignUpInteractorToPresenterInterface {
        fun responseSuccess(data: AuthResponse?)
        fun responseFailed()
    }

    interface SignUpViewToPresenterInterface {
        var view: SignUpActivity?
        var interector: SignUpInteractor?
        var router: SignUpRouter?

    }

    interface SignUpPresenterToRouterInterface {
        companion object {
            @SuppressLint("StaticFieldLeak")
            var view: SignUpActivity? = null

            fun configure(activity: SignUpActivity) {}
        }
        fun gotoShoppingMain()
    }
}
