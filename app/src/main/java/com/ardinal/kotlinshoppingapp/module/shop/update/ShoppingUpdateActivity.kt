package com.ardinal.kotlinshoppingapp.module.shop.update

import android.content.Context
import android.os.Bundle
import android.view.View
import com.ardinal.kotlinshoppingapp.R
import com.ardinal.kotlinshoppingapp.base.BaseView
import com.ardinal.kotlinshoppingapp.model.entity.EntityShop
import com.ardinal.kotlinshoppingapp.model.request.ActionShoppingRequest
import kotlinx.android.synthetic.main.activity_shopping_create.*
import kotlinx.android.synthetic.main.activity_shopping_update.*
import kotlinx.android.synthetic.main.component_toolbar.*

class ShoppingUpdateActivity : BaseView(), ShoppingUpdateContracts.ShoppingUpdatePresenterToViewInterface, View.OnClickListener {
	// MARK: Properties
	override var presenter: ShoppingUpdatePresenter? = null
	override val context: Context = this

	private var id: Int? = null

	// MARK: Lifecycle
	override fun onCreate(savedInstanceState: Bundle?) {
		ShoppingUpdateRouter.configure(this)
		super.onCreate(savedInstanceState)

		setContentView(R.layout.activity_shopping_update)

		setupView()
	}

	override fun setupView() {
		toolbar_title.text = getString(R.string.update_shopping)

		if(intent.hasExtra("KEY_UPDATE")) {
			val bundle = intent.extras!!
			id = bundle.getInt("KEY_UPDATE")
			shop_update_id.text = id.toString()
		}

		setupLink()
	}

	override fun setupLink() {
		toolbar_back_button.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		shop_update_submit.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		shop_update_date.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}
	}

	override fun onClick(view: View?) {
		when(view?.id) {
			shop_update_date.id -> {
				showDatePicker(this, shop_update_date)
			}
			toolbar_back_button.id -> {
				onBackPressed()
			}
			shop_update_submit.id -> {
				updateShopping()
			}
		}
	}

	private fun getData() : ActionShoppingRequest? {
		val date = shop_update_date.text.toString()
		val name = shop_update_name.text.toString()

		return if (date == "" || name == "") {
			null
		} else {
			val data = EntityShop(null, name, date)
			ActionShoppingRequest(data)
		}
	}

	private fun updateShopping() {
		val request = getData()

		if(request != null) {
			presenter?.interector?.submitUpdateShopping(id, request)
		}
		else {
			showToast(this, "Mohon isi semua kolom isian")
		}
	}

	fun showResponse(data: EntityShop?) {
		if(data != null) {
			val message = "Data baru = nama: ${data.name}, tanggal: ${data.date}"
			showToast(this, message)
			presenter?.router?.gotoAllShopping()
		}
		else {
			showToast(this, "Data baru gagal ditambahkan")
		}
	}
}
