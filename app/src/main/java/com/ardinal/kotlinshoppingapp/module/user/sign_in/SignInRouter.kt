package com.ardinal.kotlinshoppingapp.module.user.sign_in

import android.annotation.SuppressLint
import android.content.Intent
import com.ardinal.kotlinshoppingapp.module.shop.main.ShoppingMainActivity

class SignInRouter : SignInContracts.SignInPresenterToRouterInterface {

	companion object {
		@SuppressLint("StaticFieldLeak")
		var view: SignInActivity? = null

		fun configure(activity: SignInActivity) {
			val presenter = SignInPresenter()
			val interactor = SignInInteractor()
			val router = SignInRouter()

			activity.presenter = presenter
			presenter.view = activity
			presenter.router = router
			presenter.interector = interactor
			interactor.presenter = presenter
			view = activity
		}
	}

	override fun gotoShoppingMain() {
		val intent = Intent(view?.context, ShoppingMainActivity::class.java)
		view?.startActivity(intent)
	}
}