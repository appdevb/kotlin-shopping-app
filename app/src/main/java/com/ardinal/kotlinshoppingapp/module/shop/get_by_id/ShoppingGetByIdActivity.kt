package com.ardinal.kotlinshoppingapp.module.shop.get_by_id

import android.content.Context
import android.os.Bundle
import android.view.View
import com.ardinal.kotlinshoppingapp.R
import com.ardinal.kotlinshoppingapp.base.BaseView
import com.ardinal.kotlinshoppingapp.model.entity.EntityShop
import kotlinx.android.synthetic.main.activity_shopping_get_by_id.*
import kotlinx.android.synthetic.main.component_list_shopping.*
import kotlinx.android.synthetic.main.component_toolbar.*

class ShoppingGetByIdActivity : BaseView(), ShoppingGetByIdContracts.ShoppingGetByIdPresenterToViewInterface, View.OnClickListener {

	// MARK: Properties
	override var presenter: ShoppingGetByIdPresenter? = null
	override val context: Context = this

	// MARK: Lifecycle
	override fun onCreate(savedInstanceState: Bundle?) {
		ShoppingGetByIdRouter.configure(this)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_shopping_get_by_id)

		setupView()
	}

	override fun setupView() {
		toolbar_title.text = getString(R.string.get_shopping_by_id)

		setupLink()
	}

	override fun setupLink() {
		toolbar_back_button.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}

		shop_by_id_search.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}
	}

	override fun onClick(view: View?) {
		when(view?.id) {
			toolbar_back_button.id -> {
				onBackPressed()
			}
			shop_by_id_search.id -> {
				searchShopping()
			}
		}
	}

	private fun getData(): Int? {
		val key = shop_by_id_key.text.toString()

		return if (key == "") {
			null
		} else {
			Integer.parseInt(key)
		}
	}

	private fun searchShopping() {
		val request = getData()

		if(request != null) {
			presenter?.interector?.fetchShoppingByID(request)
		}
		else {
			showToast(this,"Tidak dapat mendapatkan shopping")
		}
	}

	fun showResponse(data: EntityShop?) {
		if(data != null) {
			list_shopping_id.text = data.id.toString()
			list_shopping_date.text = data.date.toString()
			list_shopping_name.text = data.name.toString()
		}
		else {
			showToast(this,"Tidak dapat mendapatkan shopping")
		}
	}
}
