package com.ardinal.kotlinshoppingapp.module.shop.create

import com.ardinal.kotlinshoppingapp.model.entity.EntityShop

class CreateShoppingPresenter : CreateShoppingContracts.CreateShoppingViewToPresenterInterface,
	CreateShoppingContracts.CreateShoppingInteractorToPresenterInterface {

	override var view: CreateShoppingActivity? = null
	override var interector: CreateShoppingInteractor? = null
	override var router: CreateShoppingRouter? = null

	override fun responseSuccess(data: EntityShop?) {
		view?.showResponse(data)
	}

	override fun responseFailed() {
		view?.showToast(view?.context!!,"Gagal mendapatkan response dari server")
	}
}
