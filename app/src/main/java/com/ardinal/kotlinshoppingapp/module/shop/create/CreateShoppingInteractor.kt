package com.ardinal.kotlinshoppingapp.module.shop.create

import android.annotation.SuppressLint
import android.util.Log
import com.ardinal.kotlinshoppingapp.api.ShoppingAPI
import com.ardinal.kotlinshoppingapp.base.BaseAPI
import com.ardinal.kotlinshoppingapp.model.request.ActionShoppingRequest
import io.reactivex.android.schedulers.AndroidSchedulers

class CreateShoppingInteractor : CreateShoppingContracts.CreateShoppingPresentorToInteractorInterface {

	override var presenter: CreateShoppingPresenter? = null

	@SuppressLint("CheckResult")
	override fun submitCreateShopping(request: ActionShoppingRequest) {
		BaseAPI.instance.createRequest(true)!!.create(ShoppingAPI::class.java)
			.requestCreateShopping(request).observeOn(AndroidSchedulers.mainThread()).subscribe({
				result ->

				if(result.isSuccessful) {
					val response = result.body()
					Log.d("RESULT_CONTENT", response?.data.toString())
					presenter?.responseSuccess(response?.data)
				}
				else {
					presenter?.responseFailed()
				}
			}, {
					e -> e.printStackTrace()
			})
	}

}
