package com.ardinal.kotlinshoppingapp.module.main

import android.annotation.SuppressLint
import android.content.Context

object MainContracts {

    interface MainPresenterToViewInterface {
        val context: Context
        var presenter: MainPresenter?

	    fun setupView()
	    fun setupLink()
    }

    interface MainPresentorToInteractorInterface {
        var presenter: MainPresenter?
    }

    interface MainInteractorToPresenterInterface

    interface MainViewToPresenterInterface {
        var view: MainActivity?
        var interector: MainInteractor?
        var router: MainRouter?
    }

    interface MainPresenterToRouterInterface {
        companion object {
            @SuppressLint("StaticFieldLeak")
            var view: MainActivity? = null

            fun configure() {}
        }

        fun gotoSignUp()
        fun gotoSignIn()
    }
}
