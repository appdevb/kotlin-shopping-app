package com.ardinal.kotlinshoppingapp.model.response

import com.ardinal.kotlinshoppingapp.model.entity.EntityShop
import com.ardinal.kotlinshoppingapp.model.entity.EntityUser
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AuthResponse (
	@field:SerializedName("email")
	@Expose
	var email: String?,

	@field:SerializedName("token")
	@Expose
	var token: String?,

	@field:SerializedName("username")
	@Expose
	var username: String?
)

data class AllUserResponse (
	@field:SerializedName("message")
	@Expose
	var message: String?,

	@field:SerializedName("data")
	@Expose
	var data: List<EntityUser>?
)

data class ActionShoppingResponse (
	@field:SerializedName("data")
	@Expose
	var data: EntityShop
)

data class AllShoppingResponse(
	@field:SerializedName("message")
	@Expose
	var message: String?,

	@field:SerializedName("data")
	@Expose
	var data: List<EntityShop>?
)

data class ByIdShoppingResponse (
	@field:SerializedName("data")
	@Expose
	var data: EntityShop? = null,

	@field:SerializedName("message")
	@Expose
	var message: String? = null
)

data class DeleteShoppingResponse (
	@field:SerializedName("message")
	@Expose
	val message: String? = null,

	@field:SerializedName("data")
	@Expose
	val data: String? = null
)