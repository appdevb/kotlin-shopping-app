package com.ardinal.kotlinshoppingapp.module.shop.get_all

import android.os.Bundle
import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.ardinal.kotlinshoppingapp.R;
import com.ardinal.kotlinshoppingapp.base.BaseView
import com.ardinal.kotlinshoppingapp.model.entity.EntityShop
import com.ardinal.kotlinshoppingapp.module.shop.get_all.adapter.AllShoppingAdapter
import kotlinx.android.synthetic.main.activity_get_all.*
import kotlinx.android.synthetic.main.component_toolbar.*

class ShoppingGetAllActivity : BaseView(), ShoppingGetAllContracts.ShoppingGetAllPresenterToViewInterface,
	View.OnClickListener {

	// MARK: Properties
	override var presenter: ShoppingGetAllPresenter? = null
	override val context: Context = this

	private var dataAdapter = AllShoppingAdapter()

	// MARK: Lifecycle
	override fun onCreate(savedInstanceState: Bundle?) {
		ShoppingGetAllRouter.configure(this)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_get_all)

		setupView()
		presenter?.interector?.fetchAllShopping()
	}

	override fun setupView() {
		toolbar_title.text = getString(R.string.get_all_shopping)

		setupLink()
		setupRecyclerView()
	}

	override fun setupLink() {
		toolbar_back_button.let {
			it.isClickable = true
			it.setOnClickListener(this)
		}
	}

	override fun onClick(view: View?) {
		when(view?.id) {
			toolbar_back_button.id -> {
				onBackPressed()
			}
		}
	}

	private fun setupRecyclerView() {
		get_all_recycler_view.apply {
			layoutManager = LinearLayoutManager(this@ShoppingGetAllActivity)
			adapter = dataAdapter
		}
	}

	fun onUpdateClicked(id: Int) {
		presenter?.router?.gotoUpdateShopping(id)
	}

	fun onDeleteClicked(id: Int) {
		presenter?.interector?.submitDeleteShopping(id)
	}

	fun showResponse(data: List<EntityShop>?) {
		if(data != null) {
			dataAdapter.data = data
			dataAdapter.notifyDataSetChanged()
		}
	}
}
