package com.ardinal.kotlinshoppingapp.module.shop.update

import com.ardinal.kotlinshoppingapp.model.entity.EntityShop

class ShoppingUpdatePresenter : ShoppingUpdateContracts.ShoppingUpdateViewToPresenterInterface,
	ShoppingUpdateContracts.ShoppingUpdateInteractorToPresenterInterface {

	override var view: ShoppingUpdateActivity? = null
	override var interector: ShoppingUpdateInteractor? = null
	override var router: ShoppingUpdateRouter? = null

	override fun responseSuccess(data: EntityShop?) {
		view?.showResponse(data)
	}

	override fun responseFailed() {
		view?.showToast(view?.context!!,"Gagal mendapatkan response dari server")
	}
}
