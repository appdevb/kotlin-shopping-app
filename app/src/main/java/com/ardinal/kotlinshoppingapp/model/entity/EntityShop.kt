package com.ardinal.kotlinshoppingapp.model.entity

import com.google.gson.annotations.SerializedName

data class EntityShop (

    @field:SerializedName("Id", alternate = ["id", "ID", "iD"])
    val id: Int?,

    @field:SerializedName("name")
    val name: String?,

    @field:SerializedName("createddate", alternate = ["CreatedDate", "createdDate", "Createddate"])
    val date: String?

)